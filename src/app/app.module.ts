import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppComponent} from './app.component';
import {ItemListComponent} from './item-list/item-list.component';
import {CreateFormComponent} from './create-form/create-form.component';
import {AppService} from './app.service';

@NgModule({
    declarations: [
        AppComponent,
        ItemListComponent,
        CreateFormComponent
    ],
    imports: [
        HttpModule,
        BrowserModule,
        FormsModule
    ],
    providers: [AppService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
