import {Component, OnInit} from '@angular/core';
import {Item} from './item';
import {AppService} from './app.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    title = 'Bookkeeper';
    items: Item[];
    categories: any[];
    error: any;

    constructor(private appService: AppService) {
    }

    ngOnInit() {
        this.appService.getCategories().then(data => {
            this.categories = data;
        });

        this.getItems();
    }

    getItems(): void {
        this.appService
            .getItems()
            .then(items => {
                this.items = items;
                this.items.forEach(item => {
                    const findCategory = this.categories.find(cat => {
                        return cat.id === item.category;
                    });

                    if (findCategory) {
                        item.category = findCategory.name;
                    }
                });
            })
            .catch(error => this.error = error);
    }

    onChanged(item: Item) {
        this.getItems();
    }
}
