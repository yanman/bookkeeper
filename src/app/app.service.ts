import {Injectable} from '@angular/core';
import {Headers, Http, Response, RequestOptions} from '@angular/http';

import 'rxjs/add/operator/toPromise';
import {Item} from './item';

@Injectable()
export class AppService {
    private url = 'https://t159.databoom.space/api1/b159/collections';  // URL to web api
    private headers: any;

    constructor(private http: Http) {
        this.headers = new Headers();
        this.headers.append('Authorization', 'Basic YXBwdXNlcjpuNmdjM24yUG1XVkc1TkFR');
    }

    getCategories(): Promise<Array<any>> {
        return this.http
            .get(this.url + '/categories', new RequestOptions({headers: this.headers}))
            .toPromise()
            .then((response) => {
                return response.json().d.results;
            })
            .catch(this.handleError);
    }

    getItems(): Promise<Array<Item>> {
        return this.http
            .get(this.url + '/products', new RequestOptions({headers: this.headers}))
            .toPromise()
            .then((response) => {
                return response.json().d.results as Item[];
            })
            .catch(this.handleError);
    }

    saveItem(item: Item): Promise<Item> {
        this.headers.set('Content-Type', 'application/json');
        return this.http
            .post(this.url + '/products', JSON.stringify(item), new RequestOptions({headers: this.headers}))
            .toPromise()
            .then((response) => {
                return response.json().d.results as Item[];
            })
            .catch(this.handleError);
    }

    deleteItem(item: Item): Promise<Response> {
        return this.http
            .delete(this.url + '/products(' + item.id + ')', {headers: this.headers})
            .toPromise()
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
