import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Item} from '../item';
import {AppService} from '../app.service';

@Component({
    selector: 'app-create-form',
    templateUrl: './create-form.component.html',
    styleUrls: ['./create-form.component.css']
})
export class CreateFormComponent implements OnInit {
    @Input() items: Item[];
    @Input() categories: any[];
    @Output() onChanged = new EventEmitter();
    item: Item;

    constructor(private appService: AppService) {
    }

    ngOnInit() {
        this.item = new Item();
    }

    create() {
        this.appService
            .saveItem(this.item)
            .then(item => {
                this.onChanged.emit(item);
            });
    }

}
