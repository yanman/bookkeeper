import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Item} from '../item';
import {AppService} from '../app.service';
import {isUndefined} from 'util';

@Component({
    selector: 'app-item-list',
    templateUrl: './item-list.component.html',
    styleUrls: ['./item-list.component.css']
})
export class ItemListComponent implements OnInit {
    @Input() categories: any[];
    @Input() items: Item[];
    @Output() onChanged = new EventEmitter();
    error: any;

    constructor(private appService: AppService) {
    }

    ngOnInit() {
    }

    get totalCount() {
        if (isUndefined(this.items)) {
            return 0;
        }
        return this.items.length;
    }

    get totalSum() {
        if (isUndefined(this.items)) {
            return 0;
        }

        let sum = 0;
        this.items.forEach(item => {
            sum += +item.sum;
        });
        return sum;
    }

    deleteItem(item: Item, event: any) {
        event.stopPropagation();
        event.preventDefault();
        this.appService
            .deleteItem(item)
            .then(result => {
                this.onChanged.emit(item);
            })
            .catch(error => this.error = error);
    }

}
