export class Item {
    id: string;
    title: string;
    category: number;
    count: number;
    sum: number;
    time: number;

    constructor() {
        this.count = 1;
        this.sum = 0;
        this.time = (new Date()).getTime();
    }
}
